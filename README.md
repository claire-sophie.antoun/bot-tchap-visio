# bot-tchap-visio
# Conference Bot for Matrix

##French version below

## Overview

This bot is designed to facilitate virtual conferences on the Tchap platform, utilizing BigBlueButton for video conferencing. It can generate a conference link and allows users to update the room description with the link. The bot requires administrative privileges to modify the room's description.

## Features

- **Generate Conference Link**: By sending the `!visio` command, the bot will respond with a link to the virtual conference.
- **Update Room Description**: Users can request the bot to update the room description with the conference link by sending the `!desc` command. If the bot is not an administrator, it will provide instructions on how to promote it.

## Prerequisites

- Python 3.6 or later.
- Access to a Matrix server.
- Access to a BigBlueButton server.
- Libraries including simplematrixbotlib, requests, and more (see the source code).

## Configuration

Update the following variables in your configuration file `/src/config.py.example `:

- `bot_name`: Your bot's Matrix username.
- `bot_password`: Bot's password.
- `homeserver`: Matrix server URL.
- `bbb_url`: BigBlueButton server URL.
- `bbb_secret`: BigBlueButton secret key.
- `language`: Language code for translations (e.g., 'en', 'fr').

## Installation

1. Clone the repository or download the source code.
2. Install the required dependencies:
    ```
    pip install simplematrixbotlib requests bigbluebutton_api_python
    ```
3. Rename the `config.py.example` to `config.py` and fill in the required details.
4. Run the bot script:
    ```
    python3 visio.py

    ```


#French Version:


# bot-tchap-visio
# Bot de Conférence pour Matrix

## Description

Ce bot est conçu pour faciliter les conférences virtuelles sur la plateforme Tchap en utilisant BigBlueButton pour les vidéoconférences. Il peut générer un lien de conférence et permettre aux utilisateurs de mettre à jour la description du salon avec ce lien. Le bot nécessite des privilèges d'administrateur pour modifier la description du salon.

## Fonctionnalités

- **Générer un Lien de Conférence**: En envoyant la commande `!visio`, le bot répondra avec un lien vers la conférence virtuelle.
- **Mettre à Jour la Description de la Salle**: Les utilisateurs peuvent demander au bot de mettre à jour la description de la salle avec le lien de la conférence en envoyant la commande `!desc`. Si le bot n'est pas administrateur, il fournira des instructions sur comment le promouvoir.

## Prérequis

- Python 3.6 ou ultérieur.
- Accès à un serveur Matrix.
- Accès à un serveur BigBlueButton.
- Bibliothèques incluant simplematrixbotlib, requests, et plus (voir le code source).

## Configuration

Mettez à jour les variables suivantes dans votre fichier de configuration `/src/config.py.example`:

- `bot_name`: Nom d'utilisateur de votre bot sur Matrix.
- `bot_password`: Mot de passe du bot.
- `homeserver`: URL du serveur Matrix.
- `bbb_url`: URL du serveur BigBlueButton.
- `bbb_secret`: Clé secrète de BigBlueButton.
- `language`: Code de langue pour les traductions (par exemple, 'en', 'fr').

## Installation

1. Clonez le référentiel ou téléchargez le code source.
2. Installez les dépendances requises:
    ```
    pip install simplematrixbotlib requests bigbluebutton_api_python
    ```
3. Renommez le `config.py.example` en `config.py` et remplissez les détails requis.
4. Exécutez le script du bot:
    ```
    python3 visio.py
    ```

