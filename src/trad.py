translations = {
    'en': {
        'welcome_msg': "Hello! I am the conference bot and here is the link to the conference associated to your room: {conference_link}\nIf you would like me to update the room description with this link, please send the 'desc' command.",
        'success_msg': "Successfully set the room description!",
        'need_admin_msg': "I need to be an administrator of this room to update the room description with the link.\n\nTo make me an administrator, please follow these steps:\n1. Click on the room settings.\n2. Navigate to the 'Members' section.\n3. Find my username and change my Power level to 'Admin'.\n\nOnce I'm an administrator, send the 'desc' command again, and I'll update the room description for you!",
        'failed_to_set_description': "Failed to set room description. Status Code: {status_code}. Response: {response_text}",
        'failed_to_generate_link': "Failed to create conference link. Please try again later.",
        'Description': "Link to visio-conference for room {room_name} : {conference_link}"
    },
    'fr': {
        'welcome_msg': "Bonjour! Je suis le bot de conférences et voici le lien vers la conférence associée à votre salon: {conference_link}\nSi vous souhaitez que je mette à jour la description du salon avec ce lien, veuillez envoyer la commande 'desc'.",
        'success_msg': "La description du salon a été modifiée avec succès!",
        'need_admin_msg': "Je dois être administrateur de ce salon pour mettre à jour la description du salon avec le lien.\n\nVoici les étapes à suivre pour me rendre administrateur:\n1. Cliquez sur les paramètres du salon.\n2. Accédez à la section 'Membres'.\n3. Trouvez mon nom d'utilisateur et changez mon niveau de pouvoir en 'Admin'.\n\nUne fois que je suis administrateur, envoyez la commande 'desc' à nouveau, et je mettrai à jour la description du salon pour vous!",
        'failed_to_set_description': "Échec de la modification de la description du salon. Code d'état: {status_code}. Réponse: {response_text}",
        'failed_to_generate_link': "Échec de la création du lien de la conférence associée au salon. Veuillez réessayer ultérieurement",
        'Description':"Lien vers la visio-conférence pour le salon {room_name} : {conference_link}"
    }
}
